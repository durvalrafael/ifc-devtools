/*
** utilizar o seletor $_ dentro do ifcDevTools,
** para evitar conflitos com versões anteriores do jQuery.
** por exemplo: no sitestudio utiliza-se o 1.4.2, e por isso,
** o ajaxPrefilter não funciona e não conseguimos capturar eventos
*/

function injectScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0];
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function(){
      if ( !done && (!this.readyState
           || this.readyState == 'loaded'
           || this.readyState == 'complete') ) {

        if (typeof callback === 'function') {
            done = true;
            callback();
        }

        script.onload = script.onreadystatechange = null;
        head.removeChild(script);
      }
    };

    head.appendChild(script);
}

function IfcDevTools ($_, options) {
    var $window = $_(window);
    var $document = $_(document);
    var $body = $_('body');

    var that = {
        ambient: _.get(window, 'ifcDevToolsContext.ambient', ''),
        context: _.get(window, 'ifcDevToolsContext.context', ''),
        location: _.get(window, 'ifcDevToolsContext.location', ''),
        storeId: _.get(window, 'ifcDevToolsContext.storeId', ''),
        devAmbient: _.get(window, 'ifcDevToolsContext.devAmbient', false)
    };
    var default_options = {
		appName: _.get(window, 'ifcDevToolsContext.options.appName', 'ifc-devtools'),
        selectors: {
            admin: {
                header: '.newAdminMenu',
                menuBar: '.af_menuBar_content tr:eq(0)',
                logo: '.headerLogo',
                btnRunSolr: '.run-solr-btn',
                btnRunCache: '.run-cache-btn',
                btnsWrapper: '.af_panelStretchLayout .af_panelStretchLayout'
            },
            sitestudio: {
                title: '#stores-catalogpages-content-box h5:eq(0)',
                footer: '#footer',
                templateMainLink: '.jstree .jstree-closed .jstree-icon:eq(0)',
                templateContent: '#pageContent',
                wireframeContent: '#wireframeContent',
                publishList: '#publishList',
                publishMultipleList: '#publishMultipleList',
                publishTray: '.publishTray',
                publishTrayNodes: '.publishTray .node',
                publishAllBtn: '.publish-all',
                publishPageBtn: '.publish-page',
                publishMultiBtn: '.publish-multiple-page',
                goBackBtn: '.go-back',
                addons: '.sitestudio-footer-addons'
            },
            jira: {
                createDeployTask: '.deploy-task',
                createTaskTitle: '.formtitle'
            }
        },
        elements: {
            admin: {
                btnSiteStudio: '\
                    <td class="af_menuBar_item">\
                        <a href="/icmanager/sitestudio/pagebuilder.jsp" class="inject-link" target="_blank">Sitestudio</a>\
                    </td>'
            }
        },
        api: {
            sitestudio: {
                templates: '/icmanager/api/pagebuilder/templateTree',
                template: '/icmanager/api/pagebuilder/template/{{storeId}}/{{page}}',
                solrrule: '/icmanager/api/pagebuilder/solrrule',
                component: '/icmanager/api/pagebuilder/component',
                pageComponents: '/icmanager/api/pagebuilder/template/{{storeId}}/{{page}}/components',
                save: '/icmanager/api/pagebuilder/{{page}}',
                publish: '/icmanager/api/pagebuilder/publish/{{page}}',
                publishall: '/icmanager/api/pagebuilder/publish/all',
                export: '/icmanager/api/pagebuilder/export',
            }
        },
        events: {
            sitestudio: {
                loadedContent: 'pagebuilder.page.loaded',
                // savedPage: 'pagebuilder.page.saved',
                publishPage: 'pagebuilder.page.publish',
                publishAll: 'pagebuilder.publishall',
                loadedComponents: 'pagebuilder.component.loaded',
                loadedPageTree: 'pagebuilder.pages.loaded',
                publishing: 'pagebuilder.publishing',
                publishFinish: 'pagebuilder.publishFinish'
            }
        }
    };

    function init (newOptions) {
        that.options = $_.extend(true, {}, default_options, newOptions);
        that.status = 'idle';
        that.currentPublish = '';
        that.nextPublish = [];

        if (!!window.isInfracommerce) {
            initFunctions();
            dispatchEvents();
        }

        return that;
    }

    function initFunctions() {

        triggers();

        if (that.location == 'admin') {
            if (that.ambient == 'dev') {
                $_(that.options.selectors.admin.menuBar).append(that.options.elements.admin.btnSiteStudio);
            }
            if (!!that.devAmbient) {
                var devUrl = 'http://www-' + that.storeId + '-' + that.ambient + '.infracommerce.com.br';
                var cacheUrl = devUrl + '/cache/all';

                $body.addClass(that.ambient + '-admin');

				$_(that.options.selectors.admin.logo)
					.attr('title', 'Ir para a loja (nova janela)')
					.parent().addClass('pos-relative')
					.append('<div class="selo-ambiente">' + that.ambient + '</div>');

                $_(that.options.selectors.admin.logo).on('click', function () {
					window.open(devUrl);
                });
                $document.on('click', that.options.selectors.admin.btnRunCache, function () {
                    if (!$_(this).hasClass('disabled')) {
                        window.postMessage({ type: "RUN_CACHE", url: cacheUrl }, "*");
                    }
                });

                // ------- gestão de artigos
                var hasBtn = false;
                switch (that.context) {
					case 'articles':
						window.postMessage({ type: "STATUS_SOLR", ambient: that.ambient, indice: "Article" }, "*");

                        $document.on('click', that.options.selectors.admin.btnRunSolr, function () {
							if (!$_(this).hasClass('disabled')) {
								window.postMessage({ type: "RUN_SOLR", ambient: that.ambient, indice: "Article", clean: false }, "*");
							}
							// runSolr('Article');
                        });
                        hasBtn = true;
                    break;
					case 'banners':
						window.postMessage({ type: "STATUS_SOLR", ambient: that.ambient, indice: "Banner" }, "*");

                        $document.on('click', that.options.selectors.admin.btnRunSolr, function () {
							if (!$_(this).hasClass('disabled')) {
								window.postMessage({ type: "RUN_SOLR", ambient: that.ambient, indice: "Banner", clean: true }, "*");
							}
                        });
                        hasBtn = true;
                    break;
					case 'menus':
						window.postMessage({ type: "STATUS_SOLR", ambient: that.ambient, indice: "Menu" }, "*");

                        $document.on('click', that.options.selectors.admin.btnRunSolr, function () {
							if (!$_(this).hasClass('disabled')) {
								window.postMessage({ type: "RUN_SOLR", ambient: that.ambient, indice: "Menu", clean: true }, "*");
							}
                        });
                        hasBtn = true;
                    break;
					case 'config':
						window.postMessage({ type: "STATUS_SOLR", ambient: that.ambient, indice: "Config" }, "*");

                        $document.on('click', that.options.selectors.admin.btnRunSolr, function () {
							if (!$_(this).hasClass('disabled')) {
								window.postMessage({ type: "RUN_SOLR", ambient: that.ambient, indice: "Config", clean: false }, "*");
							}
                        });
                        hasBtn = true;
                    break;
                }
                if (!!hasBtn) {
                    var $btnsWrapper = $_(that.options.selectors.admin.btnsWrapper).first();
                    var btnText = 'Rodar Índice';
					$btnsWrapper
						.prepend('\
						<div class="solr-wrapper">\
							<span class="solr-status"></span>\
                            <a class="run-solr-btn">' + btnText + '</a>\
                            <a class="run-cache-btn">Limpar cache (catálogo)</a>\
						</div>');
                }

            }
        }

        if (that.location == 'sitestudio') {

            // substituir o ajax nativo pelo mais recente, para funcionar os eventos
            $.ajax = $_.ajax;

            that.storeId = $_('#logged').html().replace('Your store: ', '');

			try{
				injectMonaco();
			}
			catch(e) {
				console.error('Erro ao inicializar o plugin do VSCODE para textarea', e);
            }

            // ---- prevent sitestudio start with error
            if (that.storeId == 'null') {
                window.history.go(0);
            } else {
                $_(that.options.selectors.sitestudio.footer)
                .html(`
                <div class="inject-label">${that.storeId}</div>
                <div class="sitestudio-footer-addons">
                    <div class="item goBack">
                        <div class="btn-default go-back"><i class="fa fa-arrow-left"></i> Voltar</div>
                    </div>
                    <div class="item publishList">
                        <select id="publishList"></select>
                        <div class="btn-default save-page"><i class="fa fa-save"></i> Save</div>
                        <div class="btn-default publish-page"><i class="fa fa-check"></i> Publish this</div>
                    </div>
                    <div class="item publishMultipleListTrigger">
                        <div class="btn-default publish-multiple-trigger"><i class="fa fa-list"></i> Publish various</div>
                    </div>
                    <div class="item publishTray">
                    </div>
                    <div class="item publishMultipleList">
                        <div class="close-btn"></div>
                        <h2>Multi-publish</h2>
                        <p>Selecione mais de um pressionando shift ou ctrl.</p>
                        <select id="publishMultipleList" multiple="true"></select>
                        <div class="btn-default publish-multiple-page"><i class="fa fa-check"></i> Publish selected</div>
                    </div>
                    <div class="item exportAll"><div class="btn-default publish-all"><i class="fa fa-check-circle"></i> Publish all</div></div>
                    <div class="item exportAll"><div class="btn-default export-pages"><i class="fa fa-download"></i> Export ZIP</div></div>
                </div>
                `);

                $_.ajax({
                    url: that.options.api.sitestudio.templates,
                    type: 'GET',
                    dataType: 'json'
                })
                .done(function (data) {
                    var templates = _.get(data, '[0].pageList', false);

                    if (!!templates) {
                        var template = `<option value="">-- Templates --</option>
                        {{#items}}
                            <option value="{{.}}">{{.}}</option>
                        {{/items}}
                        `;

                        $('#publishList').html(Mustache.render(template, { items: templates }));
                        $('#publishMultipleList').html(Mustache.render(template, { items: templates }));
                    }
                });

                $_(that.options.selectors.sitestudio.title)
                    .html('<div class="destaque">' + that.storeId + '</div> Templates');

                setTimeout(function () {
                    $_(that.options.selectors.sitestudio.templateMainLink).click();
                }, 200);
                
                checkStatus();
            }
        }

        if (that.location == 'icmlogin') {
            // ---- redirect to functional admin login
            window.location = '/admin';
        }

        if (that.location == 'solr') {
            // ---- remove dangerous buttons on SOLR
            setInterval(function () {
                if (window.location.href.indexOf('SkuIndex') > -1 &&
                    window.location.href.indexOf('dataimport') > -1) {
                    $_('button.execute').hide();
                }
            }, 300);
		}

		if (that.location == 'catalog') {
            that.buildVersion = acecIndexInformation.buildVersion || '?';
            let buildText = '';

            if (that.ambient == 'dev') {

            } else if (that.ambient == 'hcp') {
                buildText = '\nBuild: #' + that.buildVersion;
            } else {
                that.ambient = 'PRD';
                buildText = '\nBuild: #' + that.buildVersion;
            }

            console.log('%c[' + that.options.appName + ']\nAmbiente: ' + that.ambient.toUpperCase() + buildText, 'font-weight:bold;font-size:18px;');

            if (that.ambient != 'dev' && that.ambient != 'dev') {
                checkDevHcpReferences();
            }
        }

        if (that.location == 'jira') {

            if (that.context == 'createTask') {

                if ($_('.jiraform .fieldValueArea:eq(0) span').html().trim() == 'Infracommerce Infraestrutura') {
                    initDeployTask();
                }

            }
        }

        setBodyClass();

		console.log('%c[' + that.options.appName + '] ✔️ Functions started!', 'color:green;');

    }

    function setBodyClass() {
        $body.addClass('ifcdevtools-' + that.location);
    }

    function dispatchEvents() {

        var dispatchEvent = function (event, data = {}) {
            console.log('%cEvent emitted: ' + event, 'background:#000;color:chartreuse;padding:0 10px;', data);
            $window.trigger(event, [data]);
        };

        if (that.location == 'sitestudio') {
            $_.ajaxPrefilter(function (options, originalOptions, jqXHR) {

                if (!!jqXHR) {
                    jqXHR.done(function (data) {
                        if (options.url.indexOf('template/' + that.storeId) > -1) {
                            if (data.class == 'com.infracommerce.manager.to.home.CatalogPage') {
                                var page = options.url.split('/');
                                page = page[(page.length-1)];
                                dispatchEvent(that.options.events.sitestudio.loadedContent, {page: page, content: data.content, wireframe: data.wireframe });
                            }

                            if (options.url.indexOf('components') > -1) {
                                var page = options.url.split('/');
                                page = page[(page.length-2)];
                                dispatchEvent(that.options.events.sitestudio.loadedComponents, {page: page, components: data});
                            }
                        }

                        if (options.url.indexOf('publish/') > -1) {
                            if (options.url.indexOf('/all') > -1) {
                                dispatchEvent(that.options.events.sitestudio.publishAll);
                            } else {
                                var page = options.url.split('/');
                                page = page[(page.length-1)];
                                dispatchEvent(that.options.events.sitestudio.publishPage, {page: page});
                            }
                        }

                        if (!!data && !!data.buffer) {
                            if (data.buffer.class == 'java.lang.StringBuffer') {
                                if (!data.finished) {
                                    dispatchEvent(that.options.events.sitestudio.publishing, {output: data.bufferData});
                                } else {
                                    dispatchEvent(that.options.events.sitestudio.publishFinish, {output: data.bufferData});
                                }
                            }
                        }
                    });
                }

                // console.log(options, originalOptions, jqXHR);
            });
        }
    }

    function triggers() {

        $window.on(that.options.events.sitestudio.loadedContent, (e, data) => {
            let $publishList = $_(that.options.selectors.sitestudio.publishList);
            let $sitestudioAddons = $_(that.options.selectors.sitestudio.addons);
            let last = data.page;
            $publishList.val(last);
            $sitestudioAddons.addClass('selected');
        });

        $window.on(that.options.events.sitestudio.publishPage, (e, data) => {
            that.currentPublish = data.page;
            $_(that.options.selectors.sitestudio.publishTrayNodes + '[data-page="' + data.page + '"]').removeClass('finished').addClass('publishing');
            that.status = 'publishing';
            checkStatus();
        });

        $window.on(that.options.events.sitestudio.publishing, (e, data) => {
            $_(that.options.selectors.sitestudio.publishTrayNodes + '[data-page="' + data.page + '"]').removeClass('finished').addClass('publishing');
            that.status = 'publishing';
            checkStatus();
        });

        $window.on(that.options.events.sitestudio.publishFinish, (e, data) => {
            let $sitestudioAddons = $_(that.options.selectors.sitestudio.addons);

            if (!!that.currentPublish) {

                if (!!that.nextPublish.length) {
                    $_(that.options.selectors.sitestudio.publishTrayNodes + '[data-page="' + that.currentPublish + '"]').removeClass('publishing').addClass('finished');

                    $window.one(that.options.events.sitestudio.loadedContent, function (e, data) {
                        $_('#auto-update-content').html('');
                        var interval = '';
                        setTimeout(() => {
                            showLoader();
                            $.ajax({
                                type: 'POST',
                                url: Mustache.render(that.options.api.sitestudio.publish, { page: data.page }),
                                dataType: 'json',
                                data: { 'pageContent': data.content, 'wireframeContent': data.wireframe },
                                success: function(result) {
                                    $_('#stores-catalogpages-box').hide('slow');
                                    successMessage('Publishing in progress. Please see process output below!');
                                    $_('#auto-update-box').show('slow');
                                    updateProcessOutput(result.processKey);

                                    interval = window.setInterval(function doIt() { updateProcessOutput(result.processKey); }, 3000);
                                    hideLoader();
                                    
                                    successMessage('Done!');
                                }
                            });
                        }, 200);
                    });
                    setTemplate(that.nextPublish[0]);
                    that.nextPublish.splice(0, 1);
                    that.status = 'publishing';
                } else {
                    $sitestudioAddons.removeClass('publishing');
                    that.currentPublish = '';
                    that.status = 'idle';
                }
            } else {
                that.status = 'idle';
            }
            checkStatus();
        });

        $document.on('change', that.options.selectors.sitestudio.publishList, (e) => {
            let $that = $_(e.currentTarget);
            let page = $that.val();

            if (!!page) {

                $_(that.options.selectors.sitestudio.addons).addClass('selected');
                setTemplate(page);

            } else {
                $_(that.options.selectors.sitestudio.addons).removeClass('selected');
            }
        });

        $document.on('click', '.save-page', function () {
            updateAndPublishPage();
        });

        $document.on('click', that.options.selectors.sitestudio.publishAllBtn + ':not(.disabled)', function () {
            $_('#auto-update-content').html('');
            publishPages(true);
        });

        $document.on('click', that.options.selectors.sitestudio.publishPageBtn + ':not(.disabled)', function () {
            $_('#auto-update-content').html('');
            updateAndPublishPage(true);
        });

        $document.on('click', that.options.selectors.sitestudio.goBackBtn + ':not(.disabled)', function () {
            $_('#auto-update-box').hide('slow');
            $_('#stores-catalogpages-box').show('slow', function () {
                $body.removeClass('publishpage');
                window.editor.layout();
            });
            // window.editor.layout
        });

        $document.on('click', '.publish-multiple-trigger, .close-btn', function () {
            let $sitestudioAddons = $_(that.options.selectors.sitestudio.addons);
            $sitestudioAddons.toggleClass('multiple');
        });

        $document.on('click', that.options.selectors.sitestudio.publishMultiBtn, function () {
            let vals = $_(that.options.selectors.sitestudio.publishMultipleList).val();
            let $sitestudioAddons = $_(that.options.selectors.sitestudio.addons);
            let $tray = $_(that.options.selectors.sitestudio.publishTray);

            if (vals.length == 0) {
                $sitestudioAddons.removeClass('multiple');
                return false;
            }

            if (!!vals.length && vals[0] == '') {
                $sitestudioAddons.removeClass('multiple');
                return false;
            }

            if (vals.length > 0) {

                if (vals.length > 3) {
                    var confirma = confirm("Mais do que 3 itens para publica&ccedil;&atilde;o em cadeia pode travar os outros usu&aacute;rios por muito tempo. Deseja prosseguir mesmo assim?");

                    if (!confirma) {
                        return false;
                    }
                }
                that.nextPublish = vals;
                $sitestudioAddons.addClass('publishing');

                var pre = '';
                if (that.status == 'publishing') {
                    pre = '<div class="node publishing" data-page="' + that.currentPublish + '">' + that.currentPublish + '</div>';
                }

                $tray.html(
                    pre +
                    vals.map(function(el) {
                        return '<div class="node" data-page="' + el + '">' + el + '</div>';
                    }).join('') +
                    '<div class="node node-btn remove-nextpublish">Cancelar pendentes</div>'
                );

                if (that.status != 'publishing') {
                    $window.one(that.options.events.sitestudio.loadedContent, function (e, data) {
                        $_('#auto-update-content').html('');
                        setTimeout(() => {
                            updateAndPublishPage(true);
                        }, 200);
                    });
                    setTemplate(that.nextPublish[0]);
                    that.nextPublish.splice(0,1);
                }

                $sitestudioAddons.removeClass('multiple');

            }
        });

        $document.on('click', '.remove-nextpublish', function () {
            that.nextPublish = [];
            $_(that.options.selectors.sitestudio.publishTrayNodes + ':not(.finished):not(.publishing)').remove();
        });

        $document.on('click', '.export-pages', function () {
            showLoader();
            $.fileDownload(that.options.api.sitestudio.export, {
                successCallback: function (url) {
                    hideLoader();
                }
            });
        });

    }

    function checkDevHcpReferences() {
        var linkshcp = '';
        var scriptshcp = '';
        var imageshcp = '';

        if (that.ambient != 'hcp') {
            linkshcp = ', link[href*="-hcp"]';
            scriptshcp = ', script[src*="-hcp"]';
            imageshcp = ', img[src*="-hcp"]';
        }

        var $links = $_('link[href*="-dev"]' + linkshcp);
        var $scripts = $_('script[src*="-dev"]' + scriptshcp);
        var $images = $_('img[src*="-dev"]' + imageshcp);

        if (!!$links.length || !!$scripts.length || !!$images.length) {
            console.group('Scripts incorretos:');
            console.log('%c[' + that.options.appName + '] Existem referências de DEV e HCP na loja!!!', 'font-size:34px;background:red;color:#FFF;');
            if (!!$links.length) {
                console.group('LINKS - clique no primeiro elemento para encontrar no HTML');
                $links.each(function (i, e) {
                    console.log($_(e), $_(e).attr('href'));
                });
                console.groupEnd();
            }
            if (!!$scripts.length) {
                console.group('SCRIPTS - clique no primeiro elemento para encontrar no HTML');
                $scripts.each(function (i, e) {
                    console.log($_(e), $_(e).attr('src'));
                });
                console.groupEnd();
            }
            if (!!$images.length) {
                console.group('IMAGES - clique no primeiro elemento para encontrar no HTML');
                $images.each(function (i, e) {
                    console.log($_(e), $_(e).attr('src'));
                });
                console.groupEnd();
            }
            console.groupEnd();
        } else {
            console.log('%c[' + that.options.appName + '] ✔️ Nenhuma referência de DEV ou HCP encontrada!', 'color:green;');
        }

    }

	function injectMonaco() {
        /*
        ** available methods for editor:
        ** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.istandalonecodeeditor.html
        */

        var $textarea = $_(that.options.selectors.sitestudio.templateContent);

		$_.getScript("https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.10.0/min/vs/loader.js", function () {

            require.config({ paths: { 'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.10.0/min/vs' } });

            // to-do: fix error with worker import
			window.MonacoEnvironment = {
				getWorkerUrl: function (workerId, label) {
					return 'monaco-editor-worker-loader-proxy.js';
				}
			};

			require(["vs/editor/editor.main"], function () {
				// ...
				//
                //stores-catalogpages-update
                $textarea.parent('.ui-wrapper').find('.ui-resizable-handle').hide();
                $textarea.parent('.ui-wrapper').prepend(`
                    <div class="editor-options">
                        <a href="#" class="btn full-size"><i class="fa fa-expand"></i> Maximizar editor</a>
                        <a href="#" class="btn reduce-size"><i class="fa fa-compress"></i> Diminuir editor</a>
                    </div>
                    <div id="monaco"></div>
                `);

                window.editor = monaco.editor.create(document.getElementById('monaco'), {
                    value: $textarea.val(),
                    language: 'html',
                    theme: "vs-dark"
                });

                window.editor.onKeyUp(function () {
                    var content = window.editor.getValue();
                    $textarea.val(content);
                });
                window.editor.onDidBlurEditor(function () {
                    var content = window.editor.getValue();
                    $textarea.val(content);
                });

                $_('.full-size, .reduce-size').on('click', () => {
                    fullMonacoSize(window.editor, $('#monaco').parent('.ui-wrapper'));
                });

                $window.on('resize', _.debounce(() => {
                    window.editor.layout();
                }, 100));

                $window.on(that.options.events.sitestudio.loadedContent, (e, data) => {
                    window.editor.setValue(data.content);
                    window.editor.setScrollTop(0);
                    $textarea.val(data.content);
                });

			});
        });

    }

    function fullMonacoSize(editor, $container) {
        $container.toggleClass('fixed');
        editor.layout();
    }

    function initDeployTask() {
        let fillDeployTask = (e) => {
            if (!!e) {
                e.preventDefault();
            }

            let configs = {
                templates: {
                    title: '[{{storeId}}] Build #{{buildNumber}} [{{ambient}}]',
                    environment: '{{ambient}}',
                    description: 'Favor efetuar o deploy do Build #{{buildNumber}} em {{ambient}}.\n' +
                    '\n' +
                    '- Static\n' +
                    '- Catalog\n' +
                    '- Ckout\n' +
                    '- Sserv\n' +
                    '\n' +
                    'Obrigado!'
                },
                fields: {
                    title: '#summary',
                    fixVersions: '#fixVersions',
                    environment: '#environment',
                    description: '#description'
                },
                data: {
                    storeId: '',
                    buildNumber: '',
                    ambient: '',
                    dateToDeploy: '',
                }
            };

            questions:
            for (let item in configs.data) {
                var question = '';
                if (item == 'storeId') {
                    question = 'Qual o STORE ID?';
                }
                if (item == 'buildNumber') {
                    question = 'Qual o número da build a ser enviada?';
                }
                if (item == 'ambient') {
                    question = 'Qual o ambiente?';
                }
                if (item == 'dateToDeploy') {
                    question = 'Qual a data desejada para o deploy? (formato: yyyy-mm-dd)';
                }
                var retorno = prompt(question);
                if (!!retorno) {
                    configs.data[item] = retorno;
                } else {
                    alert('Algum dos requisitos para a task não foram informados!');
                    break questions;
                }
            }

            if (
                !!configs.data.storeId &&
                !!configs.data.buildNumber &&
                !!configs.data.ambient &&
                !!configs.data.dateToDeploy
            ) {
                $_(configs.fields.title).val(Mustache.render(configs.templates.title, configs.data));
                $_(configs.fields.environment).val(Mustache.render(configs.templates.environment, configs.data));
                $_(configs.fields.description).val(Mustache.render(configs.templates.description, configs.data));

                var found = false;
                $_(configs.fields.fixVersions).find('option').each((i, e) => {
                    if (($(e).text() || '').trim().indexOf(configs.data.dateToDeploy + '-1-infrashop') > -1) {
                        $(e).prop('selected', true).trigger('click');
                        found = true;
                        return false;
                    }
                });
                if (!found) {
                    var manda = confirm('Atenção: não foi encontrado pacote de INFRA criado com a data desejada. Deseja abrir o painel de versões?');
                    if (!!manda) {
                        window.open('https://lab.accurate.com.br/request/secure/project/ManageVersions.jspa?pid=10970');
                    }
                }
            }
        };

        $_(that.options.selectors.jira.createTaskTitle).append(`
            <a href="#" class="jira-default-btn deploy-task">Criar task de deploy</a>
        `);
        $_(that.options.selectors.jira.createDeployTask).on('click', function (e) {
            fillDeployTask(e);
        });

    }

    function setTemplate(page) {
        $_.ajax({
            url: Mustache.render(that.options.api.sitestudio.template, {
                storeId: that.storeId,
                page: page
            }),
            type: 'GET',
            dataType: 'json'
        })
        .done(function (data) {
            let $inputPageName = $("#stores-catalogpages-pageName");
            $inputPageName.val(page);

            if (!!data.content) {
                $(that.options.selectors.sitestudio.templateContent).val(data.content);
            }
            if (!!data.wireframe) {
                $(that.options.selectors.sitestudio.wireframeContent).val(data.wireframe);
            }
        });
    }

    function checkStatus() {
        if (that.status == 'publishing') {
            $(that.options.selectors.sitestudio.publishAllBtn)
            .add(that.options.selectors.sitestudio.publishPageBtn)
            .add(that.options.selectors.sitestudio.goBackBtn)
            .addClass('disabled');
            $body.addClass('publishpage');
        } else {
            $(that.options.selectors.sitestudio.publishAllBtn)
            .add(that.options.selectors.sitestudio.publishPageBtn)
            .add(that.options.selectors.sitestudio.goBackBtn)
            .removeClass('disabled');
        }
    }

    that.fullMonacoSize = fullMonacoSize;
    that.initDeployTask = initDeployTask;
    that.checkDevHcpReferences = checkDevHcpReferences;
    that.setTemplate = setTemplate;

    return init(options);
}

function isInfracommerce() {

    window.ifcDevToolsContext = window.ifcDevToolsContext || {};

    window.ifcDevToolsContext.options = {
        appName: 'ifc-devtools',
        contextFind: {
            admin: {
                articles: 'StaticContentManager.jspx',
                banners: 'BannerManager.jspx',
                menus: 'MenuTreeManager.jspx',
                config: 'SystemParameterManager.jspx'
            },
            jira: {
                createTask: 'CreateIssue.jspa'
            }
        }
    };

    // ------ ambiente
    if (window.location.href.indexOf('dev') > -1 ||
        (window.location.href.indexOf('-dev') > -1 ||
        window.location.href.indexOf('.dev') > -1)) {
        window.ifcDevToolsContext.ambient = 'dev';
        window.ifcDevToolsContext.devAmbient = true;
    } else if (window.location.href.indexOf('hcp') > -1 ||
                window.location.href.indexOf('-hcp') > -1 ||
                window.location.href.indexOf('.hcp') > -1) {
        window.ifcDevToolsContext.ambient = 'hcp';
        window.ifcDevToolsContext.devAmbient = true;
    } else {
        window.ifcDevToolsContext.ambient = 'unknown';
    }

    // ------ location
    if (window.location.href.indexOf('/admin') > -1) {
        window.ifcDevToolsContext.location = 'admin';
        window.ifcDevToolsContext.storeId = document.title.split('-')[0].trim().toLowerCase();
    }
    if (window.location.href.indexOf('pagebuilder.jsp') > -1) {
        window.ifcDevToolsContext.location = 'sitestudio';
    }
    if (window.location.href.indexOf('jsp/pages/login.jsp') > -1) {
        window.ifcDevToolsContext.location = 'icmlogin';
    }
    if (window.location.href.indexOf('acec-index') > -1) {
        window.ifcDevToolsContext.location = 'solr';
    }
    if (document.querySelectorAll('a[href="/request/secure/Dashboard.jspa"]').length > 0) {
        window.ifcDevToolsContext.location = 'jira';
    }
    if (!!window.infrashop) {
        window.ifcDevToolsContext.location = 'catalog';
    }

    // ------ context
    if (window.location.href.indexOf(window.ifcDevToolsContext.options.contextFind.admin.articles) > -1) {
        window.ifcDevToolsContext.context = 'articles';
    }
    if (window.location.href.indexOf(window.ifcDevToolsContext.options.contextFind.admin.banners) > -1) {
        window.ifcDevToolsContext.context = 'banners';
    }
    if (window.location.href.indexOf(window.ifcDevToolsContext.options.contextFind.admin.menus) > -1) {
        window.ifcDevToolsContext.context = 'menus';
    }
    if (window.location.href.indexOf(window.ifcDevToolsContext.options.contextFind.admin.config) > -1) {
        window.ifcDevToolsContext.context = 'config';
    }

    // ------ jira context
    if (window.location.href.indexOf(window.ifcDevToolsContext.options.contextFind.jira.createTask) > -1) {
        window.ifcDevToolsContext.context = 'createTask';
    }

    if (!window.ifcDevToolsContext.location && !window.infrashop) {
        console.log('[' + window.ifcDevToolsContext.options.appName + '] IfcDevTools will not start here. If you want to, add this URL to the context.');
        window.isInfracommerce = false;
        return false;
    }

    console.log('[' + window.ifcDevToolsContext.options.appName + '] Updated vars! IfcDevTools will start.');
    window.isInfracommerce = true;
    return true;

}

document.addEventListener('sentScriptURLs', (event) => {
    var data = event.detail;

    if (!!isInfracommerce()) {

        injectScript( data.vendors, () => {
            /* ------------ Injeção do jQuery */
            if (typeof window.jQuery === 'undefined') {
                injectScript( data.jquery, () => {
                    window.ifcDevTools = new IfcDevTools($);
                });
            } else {
                if (parseInt((jQuery.fn.jquery || '').replace(/\./g, '')) < 150) {

                    injectScript( data.jquery, () => {
                        window.$_ = $.noConflict(true);
                        console.log('%c[' + window.ifcDevToolsContext.options.appName + '] ⚠️ jQuery already in page, but too old! jQuery ' + $_.fn.jquery + ' loaded!', 'color:#edc91a;');
                        window.ifcDevTools = new IfcDevTools($_);
                    });

                } else {
                    console.log('%c[' + window.ifcDevToolsContext.options.appName + '] ✔️ jQuery already in page and up-to-date.', 'color:green;');
                    window.ifcDevTools = new IfcDevTools($);
                }
            }
        });

    }


});