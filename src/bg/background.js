// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });

//example of using a message handler from the inject scripts
chrome.extension.onMessage.addListener(
function(request, sender, sendResponse) {
  chrome.pageAction.show(sender.tab.id);
  sendResponse();
});

/* ------- PEGAR SKU DO LINK ------ */

function getSku(info, tab) {
  var url = info.linkUrl;
  var split = url.split('skuId=');

  if (split.length > 1) {
    var skuId = split[split.length-1];
    
    var element = document.createElement('input');
    element.setAttribute('type', 'text');
    element.setAttribute('id', 'tempClipboard');
    element.value = skuId;
    document.body.appendChild(element);
    // console.log(element);

    element.select();
    document.execCommand('copy');
    console.log('Copiado: ' + skuId);

    // document.querySelector('#tempClipboard').outerHTML = '';
  } else {
    alert('Não existe skuId neste link!');
  }
}

// Register context menu
chrome.runtime.onInstalled.addListener(function() {
  chrome.contextMenus.create({
    title: "Copiar skuId do link",
    contexts: ["link"],
    onclick: getSku
  });
});